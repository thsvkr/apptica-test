<?php

namespace App;

class AppticaTestHelper
{
    /**
     * @return int
     */
    public static function getApplicationId(): int
    {
        return env('APPTICA_APPLICATION_ID');
    }

    /**
     * @return int
     */
    public static function getCountryId(): int
    {
        return env('APPTICA_COUNTRY_ID');
    }

    /**
     * @return string
     */
    public static function getApiToken(): string
    {
        return env('APPTICA_API_TOKEN');
    }

    /**
     * @return string
     */
    public static function getApiEndpoint(): string
    {
        return env('APPTICA_API_ENDPOINT');
    }
}