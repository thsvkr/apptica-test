<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopPositionsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'top_positions',
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->date('date');
                $table->integer('category');
                $table->integer('position');

                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('top_positions');
    }
}