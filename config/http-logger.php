<?php

use App\Log\LogAllRequests;
use Spatie\HttpLogger\DefaultLogWriter;

return [

    /*
     * The log profile which determines whether a request should be logged.
     * It should implement `LogProfile`.
     */
//    'log_profile' => \Spatie\HttpLogger\LogNonGetRequests::class,
    'log_profile' => LogAllRequests::class,

    /*
     * The log writer used to write the request to a log.
     * It should implement `LogWriter`.
     */
    'log_writer' => DefaultLogWriter::class,

    /*
     * Filter out body fields which will never be logged.
     */
    'except' => [
        'password',
        'password_confirmation',
    ],

];
