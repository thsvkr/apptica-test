<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\TopByDateRequest;
use App\Services\TopPositionService;
use Illuminate\Http\JsonResponse;

class TopCategoryApiController extends Controller
{
    /**
     * @param TopByDateRequest $request
     * @param TopPositionService $service
     * @return JsonResponse
     */
    public function appTopCategory(TopByDateRequest $request, TopPositionService $service): JsonResponse
    {
        $date = $request->getValidatedDate();
        $data = $service->getByDate($date);

        $response = [
            'status_code' => JsonResponse::HTTP_OK,
            'message' => JsonResponse::$statusTexts[JsonResponse::HTTP_OK],
            'data' => $data,
        ];

        return new JsonResponse($response, JsonResponse::HTTP_OK);
    }
}