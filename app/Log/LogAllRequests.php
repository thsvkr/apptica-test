<?php

namespace App\Log;

use Illuminate\Http\Request;

class LogAllRequests implements \Spatie\HttpLogger\LogProfile
{
    public function shouldLogRequest(Request $request): bool
    {
        return in_array(strtolower($request->method()), ['get', 'post', 'put', 'patch', 'delete']);
    }
}