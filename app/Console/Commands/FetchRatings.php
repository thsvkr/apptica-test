<?php

namespace App\Console\Commands;

use App\AppticaTestHelper;
use App\Models\TopPosition;
use Illuminate\Console\Command;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class FetchRatings extends Command
{
    protected $signature = 'ratings:fetch';

    protected $description = 'Command description';

    public function handle()
    {
        $response = $this->makeRequest();
        $data = $response->json();
        $top = $this->filterRatings($data);

        $now = Carbon::now();
        $top = $top->map(
            function ($item) use ($now) {
                $item['created_at'] = $now;
                $item['updated_at'] = $now;

                return $item;
            }
        );

        TopPosition::insert($top->toArray());
    }

    /**
     * @return Response
     */
    private function makeRequest(): Response
    {
        return Http::get(
            AppticaTestHelper::getApiEndpoint() . '/' . AppticaTestHelper::getApplicationId() . '/' . AppticaTestHelper::getCountryId(),
            [
                'date_from' => '2020-12-20',
                'date_to' => '2020-12-22',
                'B4NKGg' => AppticaTestHelper::getApiToken(),
            ]
        );
    }

    /**
     * @param Response $response
     * @return array
     */
    private function parseResponse(Response $response): array
    {
        return $response->json('data');
    }

    /**
     * @param array $data
     * @return Collection
     */
    private function filterRatings(array $data): Collection
    {
        $top = collect();

        foreach ($data as $category => $subcategories) {
            foreach ($subcategories as $subcategory => $positions) {
                foreach ($positions as $date => $position) {
                    $highestByDate = $top->where('category', '=', $category)
                                         ->where('date', '=', $date)
                                         ->where('position', '<', $position)
                                         ->first();

                    if (!$highestByDate) {
                        $top = $top->reject(
                            function ($item) use ($category, $date, $position) {
                                return ($item['category'] === $category && $item['date'] === $date && $item['position'] >= $position);
                            }
                        );

                        $top->add(['category' => $category, 'date' => $date, 'position' => $position]);
                    }
                }
            }
        }


        return $top;
    }
}
