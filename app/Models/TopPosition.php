<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TopPosition
 * @property int category
 * @property int position
 * @package App\Models
 */
class TopPosition extends Model
{
    protected $fillable = [
        'date',
        'category',
        'position',
    ];
}