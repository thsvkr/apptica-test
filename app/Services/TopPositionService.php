<?php

namespace App\Services;

use App\Models\TopPosition;
use DateTimeInterface;
use Illuminate\Support\Collection;

class TopPositionService
{
    /**
     * @param DateTimeInterface $date
     * @return Collection
     */
    public function getByDate(DateTimeInterface $date): Collection
    {
        /** @var Collection $topPositions */
        $topPositions = TopPosition::where('date', '=', $date)->get();

        return $topPositions->mapWithKeys(function ($topPosition) {
            /** @var TopPosition $topPosition */
            return [$topPosition->category => $topPosition->position];
        });
    }
}