<?php

namespace App\Http\Requests\Api;

use DateTimeInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class TopByDateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'date' => 'required|date',
        ];
    }

    /**
     * @return DateTimeInterface
     */
    public function getValidatedDate(): DateTimeInterface
    {
        return Carbon::parse($this->validated()['date']);
    }
}